# Teste Auvo Tecnologia

This is a challenge project for a Fullstack position at [Auvo](https://www.auvo.com/).

---

# Technologies Used
- [.NET](https://dotnet.microsoft.com/en-us/download) SDK
    - v7.0.100
> As a personal challenge I used [Razor Pages](https://learn.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/razor-pages-start?view=aspnetcore-7.0&tabs=visual-studio) for the first time.

### Nuget Packages

- [Newtonsoft JSON](https://www.nuget.org/packages/Newtonsoft.Json)
    - v13.0.2

## Tools Used
- [GitLab](https://gitlab.com/)
- [JetBrains Rider](https://www.jetbrains.com/rider/)
- [Konsole](https://konsole.kde.org/)

---
# License
This project is under the [GNU GPLv3](https://gitlab.com/magnussolidus/nutcache-challenge-MagnoLomardo/-/blob/main/COPYING.txt) license.

---
# Challenge Instructions
Those can be found at the Instructions folder, alongside some sample files for testing.
The file with the entirety of the instructions can be found [here](https://gitlab.com/magnussolidus/teste-auvo-tecnologia/-/blob/main/Instru%C3%A7%C3%B5es%20Teste/Processo%20de%20sele%C3%A7%C3%A3o%20de%20desenvolvedores.docx).

---
# Project Status

My Final submission date has already passed, but since I have been learning new things I will continue to work on this project until I am satisfied with it.

- [x] Has reached Due Date?
- [x] Working on it
- [ ] Completed
- [ ] Abandoned

---

# To Do List
- [ ] Complete About Page;
- [ ] Link the Console Application at ChooseDirectory page;
- [x] Finish the Proper Calculations for the Console Application;
- [ ] Add Unit Tests using [NUnit](https://nunit.org/);
- [ ] Generate *In-Code* Documentation;
- [ ] Write a *Requirements Document*;
- [ ] Write about the *Execution Scope*;
- [x] Use DDD (Domain-Driven Design);
- [x] Use Parallelism;
- [x] Use Asynchronous Methods;
- [x] Use ASP.Net MVC for the Frontend;
