using Newtonsoft.Json;

namespace Domain.Files;

public class FileName
{
    [JsonProperty("Departmento")]
    public string Departament { get; set; }
    [JsonProperty("MesVigencia")]
    public string Month { get; set; }
    [JsonProperty("AnoVigencia")]
    public int Year { get; set; }
    
    public FileName(string input, string separator = "-")
    {

        input = RemoveFileExtension(input);

        if (input.IndexOf("-", StringComparison.Ordinal) == -1)
        {
            Console.WriteLine("O arquivo fornecido está com o nome fora do padrão.\nArquivo: {0}", input);
            return;
        }
        
        var helper = input.Trim().Split(separator);
        if (helper.Length != 3)
        {
            Console.WriteLine("Não foi possível identificar o arquivo corretamente!");
        }

        Departament = helper[0];
        Month = helper[1];
        Year = int.Parse(helper[2]);
    }

    private string RemoveFileExtension(string input)
    {
        if (input.Trim().ToLower().EndsWith(".csv"))
        {
            return input.Trim().Substring(0, input.Length - 4);
        }

        return input;
    }

    public static bool IsNameValid(string name)
    {
        return name.Split("-", 3, StringSplitOptions.TrimEntries).Length == 3;
    }
}