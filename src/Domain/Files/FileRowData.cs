using Newtonsoft.Json;

namespace Domain.Files;

public class FileRowData
{
    [JsonProperty("Código")]
    public int Code { get; set; }
    [JsonProperty("Nome")]
    public string Name { get; set; }
    [JsonProperty("Valor Hora")]
    public decimal HourlyCost { get; set; }
    [JsonProperty("Data")]
    public DateOnly Date { get; set; }
    [JsonProperty("Entrada")]
    public TimeOnly BeganAt { get; set; }
    [JsonProperty("Saída")]
    public TimeOnly FinishedAt { get; set; }
    [JsonProperty("Almoço")]
    public LunchBreak LunchBreak { get; set; }
}