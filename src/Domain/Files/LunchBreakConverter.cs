using Newtonsoft.Json;

namespace Domain.Files;

public class LunchBreakConverter : JsonConverter<LunchBreak>
{
    public override void WriteJson(JsonWriter writer, LunchBreak value, JsonSerializer serializer)
    {
        var firstPart = value.BeganAt.ToString("HH:mm");
        var secondPart = value.FinishedAt.ToString("HH:mm");
        writer.WriteValue($"{firstPart} - {secondPart}");
    }

    public override LunchBreak ReadJson(JsonReader reader, Type objectType, LunchBreak existingValue,
        bool hasExistingValue, JsonSerializer serializer)
    {
        string s = (string) reader.Value;
        return new LunchBreak(s);
    }
}