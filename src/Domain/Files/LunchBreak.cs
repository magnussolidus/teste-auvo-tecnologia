using System.Globalization;
using Newtonsoft.Json;

namespace Domain.Files;

public class LunchBreak
{
    public TimeOnly BeganAt { get; set; }
    public TimeOnly FinishedAt { get; set; }
    public TimeSpan BreakDuration { get; set; }

    public LunchBreak(string input, string separator = "-")
    {
        var helper = input.Split(separator);
        if (helper.Length != 2)
        {
            throw new Exception("Não foi possível identificar o intervalo de almoço corretamente!");
        }
        var ptBr = new CultureInfo("pt-BR");
        
        var canParse = TimeOnly.TryParse(helper[0], ptBr, DateTimeStyles.AllowWhiteSpaces, out var parsedTime);
        if (!canParse)
        {
            throw new Exception("Não foi possível identificar o horário de início do almoço!");
        }
        BeganAt = parsedTime;
        canParse = TimeOnly.TryParse(helper[1], ptBr, DateTimeStyles.AllowWhiteSpaces, out parsedTime);
        if (!canParse)
        {
            throw new Exception("Não foi possível identificar o horário de término do almoço!");
        }

        FinishedAt = parsedTime;
        BreakDuration = FinishedAt - BeganAt;
    }

    public override string ToString()
    {
        string result = JsonConvert.SerializeObject(this, Formatting.Indented, new LunchBreakConverter());
        return result;
    }
}