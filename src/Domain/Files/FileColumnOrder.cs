namespace Domain.Files;

public enum FileColumnOrder
{
    Code,
    Name,
    HourlyCost,
    Date,
    BeganAt,
    FinishedAt,
    LunchBreak
}