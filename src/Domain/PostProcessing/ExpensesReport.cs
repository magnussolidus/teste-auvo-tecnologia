using Domain.Workers;
using Newtonsoft.Json;

namespace Domain.PostProcessing;

public class ExpensesReport
{
    [JsonProperty("Departamento")]
    public string Department { get; set; }
    [JsonProperty("MesVigencia")]
    public string Month { get; set; }
    [JsonProperty("AnoVigencia")]
    public int Year { get; set; }
    [JsonProperty("TotalPagar")]
    public decimal PaymentsTotal { get; set; }
    [JsonProperty("TotalDescontos")]
    public decimal DiscountsTotal { get; set; }
    [JsonProperty("TotalExtras")]
    public decimal ExtrasTotal { get; set; }
    [JsonProperty("Funcionarios")]
    public List<Worker> Workers { get; set; }

    public ExpensesReport()
    {
        Department = "";
        Month = "";
        Workers = new List<Worker>();
    }
    
    /// <summary>
    /// Itera sobre todos os funcionários para calcular os totais de Pagamento, Descontos e Extras.
    /// Deve ser chamada somente após o calculo dos funcionários já ter sido realizado.
    /// </summary>
    public void CalculateTotalValues()
    {
        var parallelOptions = new ParallelOptions()
        {
            MaxDegreeOfParallelism = (int) Math.Round(Environment.ProcessorCount * 0.8)
        };

        Parallel.ForEach(Workers, parallelOptions, worker =>
        {
            PaymentsTotal += worker.IncomingTotal;
            var workerExtraTotal = worker.HourlyCost * ((decimal) worker.ExtraHours + (8 * worker.ExtraDays));
            ExtrasTotal += workerExtraTotal;
            var missingTotal = worker.HourlyCost * ((decimal) worker.MissingHours + (8 * worker.MissingDays));
            DiscountsTotal += missingTotal;
        });

        PaymentsTotal = decimal.Round(PaymentsTotal, 2);
        ExtrasTotal = decimal.Round(ExtrasTotal, 2);
        DiscountsTotal = decimal.Round(DiscountsTotal, 2);
    }
}