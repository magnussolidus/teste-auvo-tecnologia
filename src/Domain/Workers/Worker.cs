using System.Globalization;
using Domain.Files;
using Newtonsoft.Json;

namespace Domain.Workers;

public class Worker
{
    [JsonProperty("Nome")]
    public string Name { get; set; }
    [JsonProperty("Codigo")]
    public int Code { get; set; }
    [JsonProperty("ValorHora"), JsonIgnore]
    public decimal HourlyCost { get; set; }
    [JsonProperty("TotalReceber")]
    public decimal IncomingTotal { get; set; }
    [JsonProperty("HorasExtras")]
    public double ExtraHours { get; set; }
    [JsonProperty("HorasDebito")]
    public double MissingHours { get; set; }
    [JsonProperty("DiasFalta")]
    public int MissingDays { get; set; }
    [JsonProperty("DiasExtras")]
    public int ExtraDays { get; set; }
    [JsonProperty("DiasTrabalhados")]
    public int WorkingDays { get; set; }
    [JsonIgnore]
    public List<DateOnly> DaysWorked { get; set; }

    public void AddDayToWorker(DateOnly date)
    {
        switch (date.DayOfWeek)
        {
            case DayOfWeek.Saturday:
            case DayOfWeek.Sunday:
                ExtraDays++;
                break;
            default:
                WorkingDays++;
                break;
        }
    }

    public void AddHoursToWorker(TimeOnly clockIn, TimeOnly clockOut, LunchBreak lunchBreak)
    {
        var dailyWorkHours = 8 + lunchBreak.BreakDuration.TotalHours; // 8 + lunch break
        var expectedClockOut = clockIn.Add(TimeSpan.FromHours(dailyWorkHours));
        var hasTimeDifference = clockOut.CompareTo(expectedClockOut);

        if (hasTimeDifference > 0) // has extra hours
        {
            var difference = clockOut - expectedClockOut;
            ExtraHours += difference.TotalHours;
        }
        else if (hasTimeDifference < 0) // has missing hours
        {
            var difference = expectedClockOut - clockOut;
            MissingHours += difference.TotalHours;
        }
    }

    public void CalculateIncomingTotal()
    {
        var workingDaysTotal = HourlyCost * 8 * WorkingDays;
        var extraDaysTotal = HourlyCost * 8 * ExtraDays;
        var missingDaysTotal = HourlyCost * 8 * MissingDays * -1;
        var daysTotal =  workingDaysTotal + extraDaysTotal + missingDaysTotal;

        var extraHoursTotal = HourlyCost * (decimal) ExtraHours;
        var missingHoursTotal = HourlyCost * (decimal) MissingHours * -1;
        var hoursTotal = extraHoursTotal + missingHoursTotal;

        IncomingTotal = decimal.Round(daysTotal + hoursTotal, 2);
    }
    
    public void CalculateMissingDays(DateOnly date)
    {
        var monthWorkingDays = CalculateTotalWorkingDays(date);
        var diff = monthWorkingDays - this.WorkingDays;
        MissingDays = diff > 0 ? diff : 0;
    }

    public void CalculateExtraDays(DateOnly date)
    {
        var monthWorkingDays = CalculateTotalWorkingDays(date);
        var diff = DaysWorked.Count - monthWorkingDays;
        ExtraDays = diff > 0 ? diff : 0;
    }

    private int CalculateTotalWorkingDays(DateOnly date)
    {
        var day = new DateTime(date.Year, date.Month, 1);
        var lastDay = day.AddDays(31);
        var workingDays = 0;

        while (lastDay.Month != day.Month)
        {
            lastDay = lastDay.Subtract(TimeSpan.FromDays(1));
        }

        while (day < lastDay)
        {
            if (day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday)
            {
                day = day.AddDays(1);
                continue;
            }
        
            workingDays++;
            day = day.AddDays(1);
        }

        return workingDays;
    }
    
}