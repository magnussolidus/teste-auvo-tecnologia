using System.Text;
using System.Text.RegularExpressions;
using Domain.Files;
using Domain.PostProcessing;
using Domain.Workers;

namespace Application;

public class FileReader
{
    public List<string> Lines { get; set; }
    public List<FileRowData> FormatedLines { get; set; }
    public bool IsComplete { get; set; }
    public List<string> FilesPath { get; set; }
    private int MaxThreads { get; set; }

    public FileReader()
    {
        IsComplete = false;
        Lines = new List<string>();
        FormatedLines = new List<FileRowData>();
        FilesPath = new List<string>();
        MaxThreads = (int) Math.Round(Environment.ProcessorCount * 0.8); // Procura utilizar no máximo 80% da CPU para não travar o sistema do usuário por completo
    }
    /// <summary>
    /// Função principal que realiza o processamento dos arquivos e prepara o objeto para ser escrito no arquivo de saída
    /// </summary>
    /// <param name="directoryPath">Caminho a ser processado</param>
    /// <param name="cancellationToken">Token de cancelamento</param>
    public async Task<List<ExpensesReport>> ReadCsvFiles(string directoryPath, CancellationToken cancellationToken)
    {
        try
        {
            var result = new List<ExpensesReport>();
            FilesPath = GetFilesPath(directoryPath);

            if (FilesPath is null)
            {
                Console.WriteLine("O caminho fornecido é inválido!");
                return null;
            }
            Console.WriteLine("Getting the Files in the directory");
            if (FilesPath.Count <= 0)
            {
                Console.WriteLine("Não há arquivos a serem processados neste diretório!");
                return null;
            }

            Console.WriteLine("# of Files: {0}", FilesPath.Count);

            var options = new ParallelOptions()
            {
                MaxDegreeOfParallelism = MaxThreads
            };

            #region LoadingDataIntoMemory
            
            await Parallel.ForEachAsync(FilesPath, options, async (currentFile, ct) =>
            {
                var fileStreamOptions = new FileStreamOptions()
                {
                    Access = FileAccess.Read,
                    Mode = FileMode.Open
                };
                
                var currentFileName = new FileName(GetFileName(currentFile));
                var curFileReport = new ExpensesReport
                {
                    Department = currentFileName.Departament,
                    Month = currentFileName.Month,
                    Year = currentFileName.Year
                };

                // Decidi implementar o programa para ser compatível com a codificação UTF-08.
                using var reader = new StreamReader(currentFile, Encoding.UTF8, true, fileStreamOptions);
                
                var line = await reader.ReadLineAsync(ct);
                if (line == null)
                {
                    Console.WriteLine("Arquivo vazio! Não há nada a ser processado!");
                    return;
                }
                if (!ValidateColumnRow(line))
                {
                    Console.WriteLine(
                        "Erro de processamento:" +
                        "\nArquivo atual: {0}" +
                        "\nErro: Cabeçalho Inválido" +
                        "\nMensagem: O cabeçalho não passou na validação de formatação. Confira se a codificação do arquivo está no padrão UTF-08 e no formato adequado.",
                        currentFile);
                    return;
                }

                while ((line = await  reader.ReadLineAsync(ct)) != null)
                {
                    Lines.Add(line);
                    var rowData = GetLineData(line);
                    if (rowData is null)
                    {
                        throw new Exception($"Failed to process the data on file {currentFile}\tLine:{line}");
                    }

                    FormatedLines.Add(rowData);
                    var rowWorker = new Worker()
                    {
                        Code = rowData.Code,
                        Name = rowData.Name,
                        HourlyCost = rowData.HourlyCost,
                        DaysWorked = new List<DateOnly>()
                        {
                            rowData.Date
                        }
                    };
                    if(curFileReport.Workers.Count > 0)
                    {
                        if (CheckIfWorkerExists(curFileReport.Workers, rowWorker.Code))
                        {
                            rowWorker = curFileReport.Workers.Find(x => x.Code == rowWorker.Code);
                            rowWorker?.DaysWorked.Add(rowData.Date);
                        }
                    }
                    else
                    {
                        curFileReport.Workers.Add((rowWorker));
                    }
                    
                    // dias extras = sábado ou domingo trabalhado
                    rowWorker.AddDayToWorker(rowData.Date);
                    // hora extra = hora além das 8h + 1h de almoço de seg a sex
                    rowWorker.AddHoursToWorker(rowData.BeganAt, rowData.FinishedAt, rowData.LunchBreak);
                    
                }
                
                result.Add(curFileReport);
                Parallel.ForEach(result, options, report =>
                {
                    foreach (var worker in report.Workers)
                    {
                        worker.CalculateExtraDays(worker.DaysWorked.First());
                        worker.CalculateMissingDays(worker.DaysWorked.First());
                        worker.CalculateIncomingTotal();
                    }
                    
                    report.CalculateTotalValues();
                });
            });

            #endregion
            Console.WriteLine("Lines read: {0}", Lines.Count);
            Console.WriteLine("Processed lines: {0}", FormatedLines.Count);
            return result;
        }
        catch (DirectoryNotFoundException e)
        {
            Console.WriteLine("O caminho fornecido não existe!");
            throw;
        }
        catch (Exception e)
        {
            Console.Error.WriteLine(e.Message, e);
            throw;
        }
    }

    private bool CheckIfWorkerExists(List<Worker> source, int workerCode)
    {
        return source.Exists(x => x.Code == workerCode);
    }

    /// <summary>
    /// Recebe a linha lida do arquivo e tenta convertê-la em um objeto do tipo FileRowData.
    /// </summary>
    /// <param name="buffer">Linha a ser processada</param>
    /// <param name="separator">separador de conteúdo utilizado no arquivo. Valor padrão = ";"</param>
    /// <returns>Novo objeto FileRowData; Nulo se houver um erro na leitura da linha.</returns>
    private FileRowData? GetLineData(string buffer, string separator = ";")
    {
        var columnsContents = buffer.Split(separator);
        try
        {
            var result = new FileRowData()
            {
                Code = int.Parse(columnsContents[(int) FileColumnOrder.Code]),
                Name = columnsContents[(int) FileColumnOrder.Name],
                HourlyCost = ConvertHourlyCost(columnsContents[(int)FileColumnOrder.HourlyCost]),
                Date = DateOnly.Parse(columnsContents[(int) FileColumnOrder.Date]),
                BeganAt = TimeOnly.Parse(columnsContents[(int) FileColumnOrder.BeganAt]),
                FinishedAt = TimeOnly.Parse(columnsContents[(int)FileColumnOrder.FinishedAt]),
                LunchBreak = new LunchBreak(columnsContents[(int)FileColumnOrder.LunchBreak])
            };
            return result;
        }
        catch (Exception e)
        {
            Console.Error.WriteLine(e.Message, e);
            return null;
        }
    }

    // Caso de uso: Validar a primeira linha do arquivo a ser lido
    // Como a formatação é assegurada pelas instruções do teste, não é necessário validar todas as colunas; apenas a primeira já é o suficiente.
    // param1: row (string) com a linha a ser processada
    // param2 (opcional): separador de conteúdo utilizado no arquivo. Valor padrão = ";"
    private bool ValidateColumnRow(string row, string separator = ";")
    {
        if (string.IsNullOrWhiteSpace(row))
        {
            Console.WriteLine("Cabeçalho vazio ou nulo!");
            return false;
        }
        
        var helper = row.Split(separator);
        return helper[0].Trim().ToLower().Equals("código");
    }
    
    private List<string> GetFilesPath(string directory, string fileType = ".csv")
    {
        try
        {
            DirectoryInfo dir = new DirectoryInfo(directory);

            if (!dir.Exists)
            {
                Console.WriteLine("O Diretório fornecido não existe!");
                return null;
            }

            var result = new List<string>();
            var fileInfos = dir.GetFileSystemInfos();

            foreach (var info in fileInfos)
            {
                if (!info.Exists || !Equals(info.Extension, fileType))
                {
                    continue;
                }
                
                if (FileName.IsNameValid(info.Name))
                {
                    result.Add(info.FullName);
                }
            }

            return result;

        }
        catch (DirectoryNotFoundException d)
        {
            Console.WriteLine(d.Message, d);
            throw;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message, e);
            throw;
        }
    }

    private string GetFileName(string path)
    {
        try
        {
            if (!Path.Exists(path))
            {
                throw new DirectoryNotFoundException("O Arquivo fornecido não existe!");
            }
            return Path.GetFileNameWithoutExtension(path);
        }
        catch (DirectoryNotFoundException d)
        {
            Console.WriteLine(d.Message, d);
            throw;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message, e);
            throw;
        }
    }
    

    // Converte a string de entrada para o custo por hora do funcionário
    // param1: buffer (string) com o valor a ser processado
    private decimal ConvertHourlyCost(string input)
    {
        if (string.IsNullOrWhiteSpace(input))
        {
            Console.WriteLine("Falha na conversão do valor da hora!");
            return decimal.Zero;
        }
        var expression = @"\d+(,\d{1,2})?";
        var matches = Regex.Matches(input, expression);
        var toParse = "";
        foreach (Match match in matches)
        {
            toParse += match.Value;
        }

        return matches.Count > 1 ? decimal.Parse(toParse) / 100 : decimal.Parse(toParse);
    }
}