using System.Text;

namespace Application;

public class ResultsJsonWriter
{
    public FileInfo JsonFileInfo { get; set; }
    public string JsonContent { get; set; }

    public ResultsJsonWriter(string directoryPath, string fileName, string jsonContent)
    {
        JsonFileInfo = new FileInfo(directoryPath + fileName + ".json");
        JsonContent = jsonContent;
    }

    public async Task CreateJsonFile(CancellationToken ct)
    {
        try
        {
            var resultsFile = File.Create(JsonFileInfo.FullName);
            resultsFile.Close();
            await File.WriteAllTextAsync(JsonFileInfo.FullName, JsonContent, Encoding.UTF8, ct);

        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message, e);
            throw;
        }
    }
    
}