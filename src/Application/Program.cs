﻿// See https://aka.ms/new-console-template for more information

using System.Text;
using Application;
using Newtonsoft.Json;

var loop = true;

if (!Equals(Encoding.Default, Encoding.UTF8))
{
    Console.WriteLine("\t\t --- AVISO ---" +
                      "\nA codificação padrão deste computador *NÃO É* UTF-08." +
                      "\nEste programa foi feito para trabalhar com arquivos codificados no padrão UTF-08." +
                      "\nPara evitar problemas de processamento, assegure-se que os arquivos estarão no padrão UTF-08 antes de processá-los." +
                      "\n\t\t-------------\n");
}

while (loop)
{
    Console.WriteLine("Insira o caminho do diretório a ser processado:");
    var inputPath = Console.ReadLine();

    if (string.IsNullOrWhiteSpace(inputPath))
    {
        Console.WriteLine("Não foi possível processar o caminho fornecido.");
        return;
    }
    
    var reader = new FileReader();
    var report = await reader.ReadCsvFiles(inputPath, new CancellationToken());

    var json = JsonConvert.SerializeObject(report, Formatting.Indented);
    Console.WriteLine(string.IsNullOrWhiteSpace(json)
        ? "Não foi possível processar, verifique as mensagens de erro acima"
        : json);
    
    var writer = new ResultsJsonWriter(inputPath, "resultado", json);
    await writer.CreateJsonFile(new CancellationToken());

    Console.WriteLine("Deseja sair?\nSim - 1\nNão - Qualquer outra coisa");
    if (Console.ReadLine()!.Equals("1"))
    {
        loop = false;
    }
}
